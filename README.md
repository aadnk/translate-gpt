# Subtitle Translator

## Introduction

This Python program translates subtitle files from one language to another using OpenAI's ChatGPT, or any other language model when using the manual copy-paste mode. It handles SRT (SubRip Text) files, translating their content while maintaining synchronization with the video. Users can specify source and target languages, and invoke ChatGPT/other LLMs either through the API or manually.

## Installation

### Setting Up the Environment

Creating a dedicated environment using Conda or a virtual environment is recommended to avoid conflicts with other Python projects or system-wide packages.

#### Using Conda:

```bash
conda create --prefix ./.conda python=3.10
conda activate ./.conda
```

#### Using Virtual Environment:

```bash
python -m venv .venv
source .venv/bin/activate  # On Windows use .venv\Scripts\activate
```

### Installing Dependencies

After setting up and activating the environment, install the required dependencies.

```bash
pip install -r requirements.txt
```

## Usage

### Running the Application

Run the application with the following command:

```bash
python app.py <input_srt> <output_srt> [options]
```

#### Arguments:

- `input_srt`: Path to the input SRT file.
- `output_srt`: Path for the translated SRT file.

#### Options:

- `-h`, `--help`: Show help.
- `--source-lang SOURCE_LANG`: Set the source language (default: "Japanese").
- `--target-lang TARGET_LANG`: Set the target language (default: "English").
- `--dry-run`: Simulate output without using OpenAI.
- `--api-key API_KEY`: Use a specific OpenAI API key.
- `--client-type {api,manual}`: Choose between 'api' and 'manual' clients (default: 'api').

### Example

Translate a subtitle file from Japanese to English using the API client:

```bash
python app.py my_subtitles.srt my_translated_subtitles.srt --api-key YOUR_OPENAI_API_KEY
```

## Manual Mode

### Triggering Manual Mode

To use the manual mode, specify the `--client-type manual` option:

```bash
python app.py my_subtitles.srt my_translated_subtitles.srt --client-type manual
```

### Interaction Process

In manual mode, the application facilitates interaction with ChatGPT or other LLMs by automating parts of the translation process:

1. **Clipboard Automation**: Each segment of the subtitles is automatically copied to the clipboard.
2. **User Action**: You then paste this content into a ChatGPT interface (like a web-based chat or the OpenAI API playground) to obtain the translation.
3. **Copying Back**: Copy the translated response from ChatGPT to the clipboard.
4. **Completing the Cycle**: Return to the application and press ENTER. The program will read the translation from the clipboard and proceed with the next segment.

This mode is particularly useful for users without direct API access or those preferring a more hands-on approach.

## Additional Notes

- An OpenAI API key is necessary for API mode.
- The program includes robust error handling and logging for troubleshooting.
- For more information or to contribute, refer to the source code documentation.